from django.shortcuts import render
from .models import *




def index(request):
    return render(request, 'nrg_timetable/home1.html')

def a10(request):
    Klass = "10A"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=1)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=1)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=1)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=1)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=1)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def b10(request):
    Klass = "10B"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=2)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=2)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=2)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=2)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=2)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def c10(request):
    Klass = "10C"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=3)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=3)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=3)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=3)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=3)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def a11(request):
    Klass = "11A"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=4)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=4)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=4)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=4)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=4)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def b11(request):
    Klass = "11B"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=5)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=5)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=5)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=5)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=5)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def c11(request):
    Klass = "11C"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=6)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=6)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=6)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=6)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=6)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def a12(request):
    Klass = "12A"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=7)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=7)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=7)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=7)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=7)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def b12(request):
    Klass = "12B"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=8)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=8)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=8)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=8)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=8)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)

def c12(request):
    Klass = "12C"
    E = TimetableEntry.objects.filter(weekday=0).filter(group=9)
    T = TimetableEntry.objects.filter(weekday=1).filter(group=9)
    K = TimetableEntry.objects.filter(weekday=2).filter(group=9)
    N = TimetableEntry.objects.filter(weekday=3).filter(group=9)
    R = TimetableEntry.objects.filter(weekday=4).filter(group=9)
    context = {
        'E': E,
        'T': T,
        'K': K,
        'N': N,
        'R': R,
        'Klass': Klass
    }
    return render(request, 'nrg_timetable/home.html', context)


