from django.apps import AppConfig


class NrgTimetableConfig(AppConfig):
    name = 'nrg_timetable'
