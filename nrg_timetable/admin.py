from django.contrib import admin
from .models import *
class TimetableEntryAdmin(admin.ModelAdmin):

    ...

admin.site.register(Group)
admin.site.register(Subject)
admin.site.register(Teacher)
admin.site.register(Lesson)
admin.site.register(Room)
admin.site.register(TimetableEntry, TimetableEntryAdmin)

