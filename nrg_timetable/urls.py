
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'$^', views.index, name="index"),
    url(r'10a/$', views.a10, name="a10"),
    url(r'10b/$', views.b10, name="b10"),
    url(r'10c/$', views.c10, name="c10"),
    url(r'11a/$', views.a11, name="a11"),
    url(r'11b/$', views.b11, name="b11"),
    url(r'11c/$', views.c11, name="c11"),
    url(r'12a/$', views.a12, name="a12"),
    url(r'12b/$', views.b12, name="b12"),
    url(r'12c/$', views.c12, name="c12"),


]
