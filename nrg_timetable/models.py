from django.db import models


class Subject(models.Model):
    # Aine nimetus
    name = models.CharField(max_length=64, blank=False)
    def __str__(self):
        return self.name

class Group(models.Model):
    # Klassi või rühma tähis/nimetus
    name = models.CharField(max_length=64, blank=False)
    # Kas tegemist klassiga
    is_class = models.BooleanField(default=False)
    def __str__(self):
        return self.name

class Teacher(models.Model):
    first_name = models.CharField(max_length=64, blank=False)
    last_name = models.CharField(max_length=64, blank=False)
    def __str__(self):
        return self.first_name + ' ' +self.last_name

class Lesson(models.Model):
    subject = models.ForeignKey('Subject', on_delete=models.CASCADE)

    teacher = models.ForeignKey('Teacher', on_delete=models.CASCADE)

    def __str__(self):
            return str(self.subject) + ', ' + str(self.teacher)


class Room(models.Model):
    label = models.CharField(max_length=32, blank=False)
    name = models.CharField(max_length=64)
    def __str__(self):
        return self.label


class TimetableEntry(models.Model):
    WEEKDAYS = (
        (0, 'esmaspäev'),
        (1, 'teisipäev'),
        (2, 'kolmapäev'),
        (3, 'neljapäev'),
        (4, 'reede'),
        (5, 'laupäev'),
        (6, 'pühapäev'),
    )
    GROUPS = (
        (1, '10A'),
        (2, '10B'),
        (3, '10C'),
        (4, '11A'),
        (5, '11B'),
        (6, '11C'),
        (7, '12A'),
        (8, '12B'),
        (9, '12C'),
    )


    # Ainetund
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE)
    room = models.ForeignKey('Room', on_delete=models.CASCADE)
    weekday = models.IntegerField(choices=WEEKDAYS)
    lesson_number = models.IntegerField()
    group = models.IntegerField(choices=GROUPS)

    def __str__(self):
        return str(self.lesson.subject) + " (" + str(self.room) + ")"

